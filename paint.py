from Tkinter import *

class PaintBox( Frame ):
   def __init__( self ):
      Frame.__init__( self )
      self.pack( expand = YES, fill = BOTH )
      self.master.title( "title" )
      self.master.geometry( "300x150" )

      self.message = Label( self, text = "Drag the mouse to draw" )
      self.message.pack( side = BOTTOM )
      b = Button(self.master, text="wyczysc", command=self.clear)
      b.pack()
      
      self.myCanvas = Canvas( self , highlightthickness = 0, width = 100, height = 100, borderwidth = 2)
      self.myCanvas.pack(  )
      self.clear()
      
      self.myCanvas.bind( "<Button-1>", self.paint )
      self.myCanvas.bind( "<B1-Motion>", self.paint)

   def paint( self, event ):
      x1, y1 = ( event.x - 4 ), ( event.y - 4 )
      x2, y2 = ( event.x + 4 ), ( event.y + 4 )
      self.myCanvas.create_oval( x1, y1, x2, y2, fill = "black" )
      #self.myCanvas.create_rectangle(x1, y1, x2, y2, fill='black')

   def clear( self ):
      self.myCanvas.create_rectangle(0,0,120, 120, fill='white')
PaintBox().mainloop()