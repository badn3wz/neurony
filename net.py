from pybrain.datasets            import ClassificationDataSet
from pybrain.utilities           import percentError
from pybrain.tools.shortcuts     import buildNetwork
from pybrain.supervised.trainers import BackpropTrainer
from pybrain.structure.modules   import SoftmaxLayer
from pybrain.tools.shortcuts import buildNetwork
from pybrain.tools.xml.networkwriter import NetworkWriter
from pybrain.tools.xml.networkreader import NetworkReader

import json

f = open('json.txt', 'r')
jay = json.load(f)
DS = ClassificationDataSet(400, nb_classes=3)
DS.appendLinked(jay, 0)
DS._convertToOneOfMany(bounds=[0, 1])
print 'target array: ', DS.getField('target')
try:
	net = NetworkReader.readFrom('net.xml') 
except IOError:
	net = buildNetwork(400, 200, 3)
trainer = BackpropTrainer( net, dataset=DS, momentum=0.1, verbose=True, weightdecay=0.05)
trainer.train()
print(net.activate(jay))
NetworkWriter.writeToFile(net, 'net.xml')

