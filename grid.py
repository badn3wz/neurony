import Tkinter as tk
from Tkinter import *
import ttk
import Image, ImageDraw
import numpy as np
import json
class Gui():
    def __init__(self, root):
        self.root=root
        self.entry = tk.Entry(root)
        stvar=tk.StringVar()
        stvar.set("one")
        self.height = 150
        self.center= self.height//2
        self.width = 150


        #ramka
        frame = Frame(self.root)
        frame.grid(row=0,column=0, sticky="n")
        #kolumna 1 przyciski
        Button1=Button(frame,text="Clear", command=self.clear)
        Button1.grid(row = 1,column = 1)
        #canvas
        self.canvas=tk.Canvas(root, width=self.width, height=self.height, background='white')
        self.canvas.grid(row=0,column=1)

        

        
    #TAB
        w = ttk.Notebook(root)
        w.grid(row=0,column=0)
        #pierwszy tab - frame
        frame1=ttk.Frame()
        #pierwszy tab -content
        t1_btn0 = ttk.Button(frame1, text="Test", style="C.TButton", )
        t1_btn0.grid()
        t1_btn1 = ttk.Button(frame1, text="Zapisz jako postscript", style="C.TButton", command = self.save_ps)
        t1_btn1.grid()

        frame2=ttk.Frame()
        t2_btn0 = ttk.Button(frame2, text="H", style="C.TButton",)
        t2_btn0.grid()
        t2_btn1 = ttk.Button(frame2, text="K", style="C.TButton",)
        t2_btn1.grid()
        t2_btn2 = ttk.Button(frame2, text="Z", style="C.TButton",)
        t2_btn2.grid()


        #dodanie do taba
        w.add(frame1, text='testing', state='normal')
        w.add(frame2, text='learning', state='normal')


        #malowanie
        self.canvas.bind( "<Button-1>", self.paint )
        self.canvas.bind( "<B1-Motion>", self.paint)

    def paint( self, event ):
        x1, y1 = ( event.x - 5 ), ( event.y - 5 )
        x2, y2 = ( event.x + 5 ), ( event.y + 5 )
        self.canvas.create_oval( x1, y1, x2, y2, fill = "black" )
        #self.canvas.create_rectangle(x1, y1, x2, y2, fill='black')

    def clear( self ):
        self.canvas.create_rectangle(0,0,self.width+1, self.height+1, fill='white')
        
    def save_ps(self):
        size = 20, 20
        self.canvas.postscript(file="file_name.ps", colormode='color')
        im = Image.open('file_name.ps')
        im.thumbnail(size, )#resample=Image.NEAREST)
        im.save('file.bmp')
        data = np.asarray(im)
        #np.savetxt("array.txt", data)
        #print(np.mean(data[1][1]))
        lista = list()
        for i in data:
            helper_list = list()
            for j in i:
                lista.append(np.mean(j))
            #     helper_list.append(np.mean(j))
            # lista.append(helper_list)
        fjson = open('json.txt', 'w')
        json.dump(lista, fjson)
        fhuman = open('human.txt', 'w')
        # print(lista)
        for item in lista:
            print>>fhuman, item


        #Grid.columnconfigure(self.root,1,weight=1, size=200)
if __name__== '__main__':
    root=tk.Tk()
    gui=Gui(root)
    root.mainloop()