# -*- coding: utf-8 -*-
import Tkinter as tk
from Tkinter import *
import Image, ImageDraw, json, ttk
import numpy as np
import csv
from pybrain.datasets            import ClassificationDataSet
from pybrain.utilities           import percentError
from pybrain.tools.shortcuts     import buildNetwork
from pybrain.supervised.trainers import BackpropTrainer
from pybrain.structure.modules   import SoftmaxLayer
from pybrain.tools.shortcuts import buildNetwork
from pybrain.tools.xml.networkwriter import NetworkWriter
from pybrain.tools.xml.networkreader import NetworkReader



class Gui():

    DS = ClassificationDataSet(400, nb_classes=3)
    try:
        net = NetworkReader.readFrom('net.xml') 
    except IOError:
        net = buildNetwork(400, 50, 3)
    trainer = BackpropTrainer( net, dataset=DS, learningrate=0.0001, momentum=0.3, verbose=True, weightdecay=0.01)
    

    tf = open('ds.csv','r')
    for line in tf.readlines():
        data = [float(x) for x in line.strip().split(',') if x != '']
        indata =  tuple(data[:400])
        outdata = tuple(data[400:])
        DS.addSample(indata,outdata)
    #print(DS)

    def __init__(self, root):
        self.root=root
        self.entry = tk.Entry(root)
        stvar=tk.StringVar()
        stvar.set("one")
        self.height = 200
        self.center= self.height//2
        self.width = 200
    


        #ramka
        frame = Frame(self.root)
        frame.grid(row=0,column=0, sticky="n")
        #kolumna 1 przyciski
        Button1=Button(frame,text="Czyść", command=self.clear)
        Button1.grid(row = 1,column = 1)
        #canvas
        self.canvas=tk.Canvas(root, width=self.width, height=self.height, background='white')
        self.canvas.grid(row=0,column=1)

        

        
        #TAB
        w = ttk.Notebook(root)
        w.grid(row=0,column=0)
        #pierwszy tab - frame
        frame1=ttk.Frame()
        #pierwszy tab -content
        t1_btn0 = ttk.Button(frame1, text="Test", style="C.TButton", command = self.testuj)
        t1_btn0.grid()
        self.lbl=Label(frame1, text='Wynik: ')
        self.lbl.grid()
        #t1_btn1 = ttk.Button(frame1, text="Zapisz jako postscript", style="C.TButton", command = self.save_ps)
        #t1_btn1.grid()

        frame2=ttk.Frame()
        t2_btn0 = ttk.Button(frame2, text="H", style="C.TButton", command = lambda: self.nauczaj(0))
        t2_btn0.grid()
        t2_btn1 = ttk.Button(frame2, text="K", style="C.TButton", command = lambda: self.nauczaj(1))
        t2_btn1.grid()
        t2_btn2 = ttk.Button(frame2, text="Z", style="C.TButton", command = lambda: self.nauczaj(2))
        t2_btn2.grid()
        t2_btn3 = ttk.Button(frame2, text="TRENUJ", style="C.TButton", command = self.trenuj)
        t2_btn3.grid()


        #dodanie do taba
        w.add(frame1, text='Testowanie', state='normal')
        w.add(frame2, text='Uczenie', state='normal')


        #malowanie
        self.canvas.bind( "<Button-1>", self.paint )
        self.canvas.bind( "<B1-Motion>", self.paint)










    def paint( self, event ):
        x1, y1 = ( event.x - 10 ), ( event.y - 10 )
        x2, y2 = ( event.x + 10 ), ( event.y + 10 )
        self.canvas.create_oval( x1, y1, x2, y2, fill = "black" )
        #self.canvas.create_rectangle(x1, y1, x2, y2, fill='black')

    def clear( self ):
        self.canvas.create_rectangle(0,0,self.width+1, self.height+1, fill='white')
        
    def save_ps(self):
        size = 20, 20
        self.canvas.postscript(file="file_name.ps", colormode='color')
        im = Image.open('file_name.ps')
        im.thumbnail(size, )#resample=Image.NEAREST)
        im.save('file.bmp')
        data = np.asarray(im)
        #np.savetxt("array.txt", data)
        #print(np.mean(data[1][1]))
        lista = list()
        for i in data:
            helper_list = list()
            for j in i:
                lista.append(np.mean(j))
            #     helper_list.append(np.mean(j))
            # lista.append(helper_list)
        fjson = open('json.txt', 'w')
        json.dump(lista, fjson)
        fhuman = open('human.txt', 'w')
        # print(lista)
        for item in lista:
            print>>fhuman, item
        return lista



    def testuj(self):
        lista=self.save_ps()
        ind = self.net.activate(lista)
        print(ind)
        if ind[0] > ind[1] and ind[0] > ind[2]:
            self.lbl['text'] = 'Wynik: H'
        elif ind[1] > ind [0] and ind[1] > ind[2]:
            self.lbl['text'] = 'Wynik: K'
        else:
            self.lbl['text'] = 'Wynik: Z'
            

    def nauczaj(self, tg):
        lista=self.save_ps()
        self.DS.appendLinked(lista, [tg])
        # print(self.DS)
        # print 'target array: ', self.DS.getField('target')
        # self.DS._convertToOneOfMany(bounds=[0, 1])
        # self.trainer.train()
        # self.DS._convertToClassNb()
        #print(self.DS)
        self.clear()


    def trenuj(self):
        self.DS._convertToOneOfMany(bounds=[0, 1])
        # self.trainer.trainUntilConvergence(maxEpochs=200)
        self.trainer.trainEpochs(1000)
        self.DS._convertToClassNb()
        NetworkWriter.writeToFile(self.net, 'net.xml')
        #fds = open('ds.txt', 'w')
        #json.dump(self.DS, fds)
        #lista=list()
        with open('ds.csv', 'wb') as csvfile:
            spamwriter = csv.writer(csvfile, delimiter=',')
            for inpt, trgt in self.DS:
                #lista.append([inpt.tolist(), trgt])
                lista=inpt.tolist()
                lista.append(trgt[0])
                spamwriter.writerow(lista)
                
        # print(type(inpt.tolist()))
        # json.dump(lista, fds)

        #Grid.columnconfigure(self.root,1,weight=1, size=200)
if __name__== '__main__':
    root=tk.Tk()
    gui=Gui(root)
    root.mainloop()